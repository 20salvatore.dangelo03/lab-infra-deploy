#!/bin/bash
oc login --server=$OPENSHIFT_HOST --token=$OPENSHIFT_TOKEN --insecure-skip-tls-verify

if [ $CI_COMMIT_BRANCH == "dev" ]; then
    echo "Deploying for dev"
    oc project dev
    oc apply -f ./dev
elif [ $CI_COMMIT_BRANCH == "test" ]; then
    echo "Deploying for test"
    oc project test
    oc apply -f ./test
else
    echo "Deploying for prod"
    oc project prod
    oc apply -f ./prod
fi